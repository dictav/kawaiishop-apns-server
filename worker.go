package main

import (
	"fmt"
	"github.com/benmanns/goworker"
	"github.com/virushuo/Go-Apns"
	"os"
	"time"
)

var apn *apns.Apn
var identifiers map[string]int

func main() {
	identifiers = make(map[string]int)
	a, err := apns.New("cert.pem", "key.pem", "gateway.sandbox.push.apple.com:2195", 24*time.Hour)
	if err != nil {
		fmt.Printf("connect error: %s\n", err.Error())
		os.Exit(1)
	}

	go readError(a.ErrorChan)
	apn = a

	if err := goworker.Work(); err != nil {
		fmt.Println("Error:", err)
	}
}

func readError(errorChan <-chan error) {
	for {
		apnerror := <-errorChan
		fmt.Println(apnerror.Error())
	}
}
