package main

import (
	"fmt"
	apns "github.com/anachronistic/apns"
	"os"
)

func main() {
	fmt.Println("- connecting to check for deactivated tokens (maximum read timeout =", apns.FEEDBACK_TIMEOUT_SECONDS, "seconds)")

	client := apns.NewClient("gateway.sandbox.push.apple.com:2195", "cert.pem", "key.pem")
	go client.ListenForFeedback()

	for {
		select {
		case resp := <-apns.FeedbackChannel:
			fmt.Println("- recv'd:", resp.DeviceToken)
		case <-apns.ShutdownChannel:
			fmt.Println("- nothing returned from the feedback service")
			os.Exit(1)
		}
	}
}
