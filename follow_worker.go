package main

import (
	"fmt"
	"github.com/benmanns/goworker"
	"github.com/virushuo/Go-Apns"
)

func init() {
	goworker.Register("LoveShop", followWorker)
}

func followWorker(queue string, args ...interface{}) error {
	v := args[0]
	m, ok := v.(map[string]interface{})
	if ok {
		fmt.Println("Follow")
		fmt.Println(m)
		name := m["user"].(string)
		shopName := m["shop"].(string)
		token := m["token"].(string)

		payload := apns.Payload{}
		payload.Aps.Alert.LockKey = "LoveShop"
		payload.Aps.Alert.LockArgs = []string{name, shopName}

		notification := apns.Notification{}
		notification.DeviceToken = token
		notification.Identifier = 0
		notification.Payload = &payload
		err := apn.Send(&notification)
		fmt.Printf("send id(%x): %s\n", notification.Identifier, err)
	} else {
		fmt.Println("Fail hello notification")
		fmt.Println("fail")
	}

	return nil
}
